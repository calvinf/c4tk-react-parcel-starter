# React Starter Using Parcel

## Environment setup

* Install [Node.js](https://nodejs.org/download/)
* Fork this repository
* Clone your fork to your computer
* Run `npm install` in the directory
* Run `npm start` to launch `index.html` in development mode

## Getting Started

* `index.html` is your main HTML file where the [React](https://reactjs.org/) app will be initialized
* `assets/hello.js` contains a starting point for your [React](https://reactjs.org/) app
* `assets/hello.scss` contains a starting point for your styles using [SCSS (Sass)](http://sass-lang.com/).

## Running

* Dev: `npm start` -- Parcel supports "Hot Module Reloading" so you can save an update, open the browser, and it will automatically update
* Build: `npm build` -- generate the "production" build
* Serve: `npm serve` -- serves the "production" build

## License

Public domain, unlicensed software, see LICENSE file.

## Video Inspiration

Props to Elijah Manor for his Manorisms video [Bootstrap a React App with Parcel](https://www.youtube.com/watch?v=ybjmUgKW3vU) which inspired this starter.
